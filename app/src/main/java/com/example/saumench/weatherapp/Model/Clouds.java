package com.example.saumench.weatherapp.Model;

/**
 * Created by Saumench on 26/09/2017.
 */

public class Clouds {
    private int all;

    public Clouds(int all) {
        this.all = all;
    }

    public int getAll() {
        return all;
    }

    public void setAll(int all) {
        this.all = all;
    }
}

