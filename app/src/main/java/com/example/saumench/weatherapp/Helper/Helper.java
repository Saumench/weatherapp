package com.example.saumench.weatherapp.Helper;

import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Saumench on 26/09/2017.
 */

public class Helper {
    static String stream = null;

    public Helper() {
    }

    /*THIS FUNCTION TO MAKE A REQUEST TO OpenWeatherMap API AND GET A RETURN RESULT*/
    public String getHTTPData(String urlString) {
        try {
            URL url = new URL(urlString);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            if (httpURLConnection.getResponseCode() == 200) /*OK 200*/ {
                BufferedReader reader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null)
                    stringBuilder.append(line);
                stream = stringBuilder.toString();
                httpURLConnection.disconnect();
            }
        } catch (Exception e) {
            Log.e("Helper Exception", e.getMessage());
        }
        return stream;
    }
}
