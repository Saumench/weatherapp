package com.example.saumench.weatherapp;

import android.support.annotation.LayoutRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;

import com.example.saumench.weatherapp.R;

/**
 * Created by Saumench on 07/09/2017.
 */

public class BaseActivity extends AppCompatActivity {

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);

        View toolbar = findViewById(R.id.toolbar);
        if (toolbar != null) setupToolBar((Toolbar) toolbar);

    }

    protected void setupToolBar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        toolbar.setContentInsetsAbsolute(0, 0);
        setupActionBar(getSupportActionBar());
    }

    protected View setupActionBar(ActionBar actionBar) {
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.toolbar);
        View view = actionBar.getCustomView();
        Toolbar.LayoutParams params = (Toolbar.LayoutParams) view.getLayoutParams();
        params.height = ViewGroup.LayoutParams.MATCH_PARENT;
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        view.setLayoutParams(params);
        return view;
    }
}
