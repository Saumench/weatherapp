package com.example.saumench.weatherapp.Common;

import android.content.Context;
import android.support.annotation.NonNull;

import com.example.saumench.weatherapp.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Saumench on 26/09/2017.
 */

public class Common {
    public static String API_KEY = "fbdd8c9d2a6606fb4cdb5305d23ead59"; /*API KEY from OpenWeatherMap*/
    public static String API_LINK = "http://api.openweathermap.org/data/2.5/weather"; /*API LINK from OpnWeatherMap*/

    @NonNull
    /*THIS FUNCTION CREATES A FUNCTIONAL LINK TO THE API PATH*/
    public static String apiRequest(String lat, String lon) {
        StringBuilder stringBuilder = new StringBuilder(API_LINK);
        stringBuilder.append(String.format("?lat=%s&lon=%s&APPID=%s&units=metric", lat, lon, API_KEY));
        return stringBuilder.toString();
    }

    /*THIS FUNCTION CONVERT UNIX TIME STAMP TO DATE TIME WITH FORMAT HH:mm*/
    public static String unixTimeStampToDateTime(Context context, double unixTimeStamp) {
        DateFormat dateFormat = new SimpleDateFormat(context.getString(R.string.time_format));
        Date date = new Date();
        date.setTime((long) unixTimeStamp * 1000);
        return dateFormat.format(date);
    }

    /*THIS FUNCTION GET A LINK IMAGE FROM OpenWeatherMap*/
    public static String getImage (Context context, String icon){
        return String.format(context.getString(R.string.icon_image), icon);
    }

    /*THIS FUNCTION GET DATE WITH FORMAT "dd MMMM yyyy HH:mm"*/
    public static String getDate(Context context){
        DateFormat dateFormat = new SimpleDateFormat(context.getString(R.string.date_format));
        Date date = new Date();
        return dateFormat.format(date);
    }

}
